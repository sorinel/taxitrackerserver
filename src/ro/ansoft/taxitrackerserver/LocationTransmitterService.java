package ro.ansoft.taxitrackerserver;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class LocationTransmitterService extends Service {

	private LocationManager locationManager;
	private LocationListener locationListener;
	private JSONParser jsonParser;
	private String TAG = "LocationTransmitterService";
	private static final String urlUploadCoordinates = "http://ansoft.ro/taxi/set_coordinates.php";

	@Override
	public void onCreate() {
		Log.d(TAG, "On create -B");
	    super.onCreate();
	    locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
	    jsonParser = new JSONParser();

	    locationListener = new LocationListener() {
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {}

			@Override
			public void onProviderEnabled(String provider) {}

			@Override
			public void onProviderDisabled(String provider) {}

			@Override
			public void onLocationChanged(Location location) {
				transmittLocation(location.getLatitude(), location.getLongitude());
				Log.d(TAG, "Location changed: " + location.getLatitude() + ", " + location.getLongitude());
			}
		};

	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "On start command -B");
		final long minTime = 5000; // 5 seconds
		final float minDistance = 15f; // 15 meters
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListener);
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
    public IBinder onBind(Intent intent) {
	    return null;
    }

	@Override
	public void onDestroy() {
		Log.d(TAG, "On destroy -B");
	    super.onDestroy();
	    locationManager.removeUpdates(locationListener);
	}

	private void transmittLocation(final double latitude, final double longitude) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if(isOnline()) {
					uploadData(latitude, longitude);
				}
			}
		}).start();
	}

	private boolean isOnline() {
		ConnectivityManager connec = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connec != null && (connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) || 
		    (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)) { 
		        return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
		         connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED ) {            
				return false;
		}
		return false;
	}

	private void uploadData(double latitude, double longitude) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(MainActivity.LAT, "" + latitude));
        params.add(new BasicNameValuePair(MainActivity.LONG, "" + longitude));
        JSONObject json = jsonParser.makeHttpRequest(urlUploadCoordinates, JSONParser.POST, params);

        updateLog(latitude, longitude);
        if(json == null) {
        	Log.d(TAG, "json null");
        }
	}

	private void updateLog(double latitude, double longitude) {
		Bundle bundle = new Bundle();
		bundle.putDouble(MainActivity.LAT, latitude);
		bundle.putDouble(MainActivity.LONG, longitude);

        Message message = new Message();
        message.what = MainActivity.UPDATE_LOG;
        message.setData(bundle);

        MainActivity.handler.sendMessage(message);
	}

}
