package ro.ansoft.taxitrackerserver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {

	private ImageButton mStartButton;
	private ImageButton mStopButton;
	private TextView mStatusTextView;
	private EditText mLogEditText;
	static Handler handler;
	static final int UPDATE_LOG = 1;
	static final String LAT = "lat";
	static final String LONG = "long";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mStartButton = (ImageButton) findViewById(R.id.startButton);
		mStopButton = (ImageButton) findViewById(R.id.stopButton);
		mStatusTextView = (TextView) findViewById(R.id.status);
		mLogEditText = (EditText) findViewById(R.id.log);
		handler = new Handler(new Callback() {
			@Override
			public boolean handleMessage(final Message message) {
				if(message.what == UPDATE_LOG) {
					MainActivity.this.runOnUiThread(new Runnable() {
	                    @Override
						public void run() {
		                    double latitude = message.getData().getDouble(LAT);
		                    double longitude = message.getData().getDouble(LONG);
		                    mLogEditText.append("\n" + latitude + " " + longitude);
	                    }
                    });
				}
				return true;
			}
		});

		mStartButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mStatusTextView.setText(getString(R.string.status_transmitting));
				mStartButton.setEnabled(false);
				startService(new Intent(MainActivity.this, LocationTransmitterService.class));
			}
		});
		
		mStopButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mStatusTextView.setText(getString(R.string.status_not_transmitting));
				mStartButton.setEnabled(true);
				stopService(new Intent(MainActivity.this, LocationTransmitterService.class));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
